package com.craterzone.democamera;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback, Camera.PictureCallback {

    private Camera camera;
    private SurfaceView surfaceView;
    private byte[] image;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    void init() {
        surfaceView = (SurfaceView) findViewById(R.id.surface);
        findViewById(R.id.click).setOnClickListener(this);
        findViewById(R.id.discard).setOnClickListener(this);
        SurfaceHolder surface = surfaceView.getHolder();
        surface.addCallback(this);
        surface.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera == null) {
            try {
                camera = Camera.open();

                camera.setPreviewDisplay(surfaceView.getHolder());
                camera.startPreview();
            } catch (Exception e) {

            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
     //   camera.release();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.click:
                takeClick();
                break;
            case R.id.discard:
                finish();
                break;
        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
      /*  Camera.Parameters parameters = camera.getParameters();
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();*/
       Camera.Parameters parameters = camera.getParameters();
        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
        // You need to choose the most appropriate previewSize for your app
      //  Camera.Size previewSize = new .setPreviewSize(myBestSize.width, myBestSize.height);
                // .... select one of previewSizes here
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        Camera.Size cs = sizes.get(0);
        parameters.setPreviewSize(cs.width, cs.height);
        //camera.setParameters(parameters);
        if (display.getRotation() == Surface.ROTATION_0) {
            parameters.setPreviewSize(height, width);
            camera.setDisplayOrientation(90);
        }

        if (display.getRotation() == Surface.ROTATION_90) {
            parameters.setPreviewSize(width, height);
        }

        if (display.getRotation() == Surface.ROTATION_180) {
            parameters.setPreviewSize(height, width);
        }

        if (display.getRotation() == Surface.ROTATION_270) {
            parameters.setPreviewSize(width, height);
            camera.setDisplayOrientation(180);
        }
//        camera.setParameters(parameters);
        if (camera != null) {
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();

            } catch (IOException e) {
                //  Toast.makeText(CameraActivity.this, "Unable to start camera preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.d("test", "picture taken");
        image = data;
        showDiloag();

    }

    private void takeClick() {
        camera.takePicture(null, null, this);
    }

    private void showDiloag() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("What do you want to do???");
        progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, "retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                camera.startPreview();
                progressDialog.dismiss();


            }
        });
        progressDialog.setButton(ProgressDialog.BUTTON_POSITIVE, "save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeByteArray(image, 0, image.length, options); //Convert bytearray to bitmap
                showDialogForSaved();
                progressDialog.dismiss();


            }
        });
        progressDialog.setButton(ProgressDialog.BUTTON_NEUTRAL, "cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                progressDialog.dismiss();
                camera.startPreview();
            }
        });

        progressDialog.show();


    }

    private void showDialogForSaved() {
        final Dialog progressDialog = new Dialog(this);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setTitle("What do you want to do???");
        ((ImageView) (progressDialog.findViewById(R.id.image))).setImageBitmap(Bitmap.createScaledBitmap(bitmap, 700, 700, false));
        progressDialog.findViewById(R.id.b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.startPreview();
                progressDialog.dismiss();
            }
        });

        progressDialog.show();
    }
}
